# GWAS data
GWAS_CAA = '../data/gwas-catalog-associations.tsv' #gwas catalog associations
GWAS_CAA_ON = '../data/gwas-catalog-associations_ontology-annotated.tsv' # gwas catalog associations ontology-annotated
GWAS_CAS = '../data/gwas-catalog-studies.tsv' # gwas catalog studies
GWAS_CAS_ON = '../data/gwas-catalog-studies_ontology-annotated.tsv' # gwas catalog studies ontology annotated
GWAS_EFO_MAP = '../data/gwas-efo-trait-mappings.tsv' # gwas efo trait mappings.tsv
JSON_AI_DOMAINS = '../data/json_ai_domains.json'

## CTCF data:
# Bed file containing the CTCF anchors
CTCF_ANCHORS = '../data/GM12878.CTCF.clusters_PET4_motifs.merged_anchors.txt'
# Serialized pickle object containing the SNP enrichment of the anchors.
ANCHORS_SNP_ENRICHMENT = '../results/anchors_snp_enrichment.pkl'


# Disease traits data
AI_DISEASE_TRAITS = '../data/AI_disease_traits.txt'
AI_DISEASE_TRAITS_RESTRICTED = '../data/AI_disease_traits_restricted.txt'
AI_DISEASE_RA = '../data/AI_disease_RA_only.txt'
AI_DISEASE_T1D = '../data/AI_disease_T1D.txt'
ALL_DISEASES = '../data/all_diseases.txt'
GENE_LIST_KRYSTIAN = '../data/gene_list_krystian.txt'

CCD_HG38_GENES = '../data/genes_hg38/GM12878_CCD_hg38_genes.bed'
RAO_HG38_GENES = '../data/genes_hg38/GM12878_rao_hg38_genes.bed'
IMR90_HG38_GENES = '../data/genes_hg38/IMR90_hg38_genes.bed'




# results:
#HG19
RESULTS_HG19 = '../results/hg_19/'
SNPs_HG19 = '../results/hg_19/SNPs/'
SNPs_HG19_AI = '../results/hg_19/SNPs/SNPs_HG19_AI.bed'
SNPs_HG19_RA = '../results/hg_19/SNPs/SNPs_HG19_RA.bed'
SNPs_HG19_T1D = '../results/hg_19/SNPs/SNPs_HG19_T1D.bed'




#HG38:

    #ALL:

RESULTS_HG38_ALL = '../results/hg_38/ALL/RES/'
SNPs_HG38_ALL = '../results/hg_38/ALL/SNPs/'
DOMAIN_GENES_RES_ALL ='../results/hg_38/ALL/DOMAIN_GENES/'

    #AI

RESULTS_HG38_AI = '../results/hg_38/AI/RES/'
SNPs_HG38_AI = '../results/hg_38/AI/SNPs/'
DOMAIN_GENES_RES_AI ='../results/hg_38/AI/DOMAIN_GENES/'
# Data after interection with external genes
DOMAINS_EXTERNAL_GENES_AI = '../results/hg_38/AI/DOMAINS_EXTERNAL_GENES/' # Domains for which external genes have been found to be presnet


    #RA:
RESULTS_HG38_RA = '../results/hg_38/RA/RES/'
SNPs_HG38_RA = '../results/hg_38/RA/SNPs/'
DOMAIN_GENES_RES_RA ='../results/hg_38/RA/DOMAIN_GENES/'

    #T1D:

RESULTS_HG38_T1D = '../results/hg_38/T1D/RES/'
SNPs_HG38_T1D = '../results/hg_38/T1D/SNPs/'
DOMAIN_GENES_RES_T1D ='../results/hg_38/T1D/DOMAIN_GENES/'


# Domains data
CCD = '../data/bed_hg38/GM12878_CCD_hg38.bed'
RAO = '../data/bed_hg38/GM12878_rao_hg38.bed'
IMR90 = '../data/bed_hg38/IMR90_hg38.bed'

#Mongo DBs and collections
AI_PANEL = 'AI_panel'
AI_GWAS_ALL = 'AI_GWAS_ALL'

# File mapping GWAS Cathalog SNP coordinates from hg38 to hg19 
SNP_mapping_file = '../data/SNP_hg38_hg19.pkl'
