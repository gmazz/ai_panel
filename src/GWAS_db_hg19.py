from utils import *

'''
Description:

    This script generates a mongoDB database containing the GWAS informations obtained from the
    GWAS cathalog, but converting these data into hg19 coordinates. Hence:

    1) Only entries with convertible SNPs (hg38 -> hg19) are included in the DB generates
    2) Domain relates data are also included 
'''

'''
######################### Function utilities for one-time use ########################:

# Functions used sporadically, for example functions used to generate an output data file that is further used without the need to execute the function multiple times.

* get_disease_list() returns the list of common immune diseases between the GWAS EFO trait mapping file, and the catalog of GWAS association file (ontology annotated).

* The auto-immune diseases from this file need to be manually selected (any data available),
editing the 'AI_disease_traits.txt'
'''

def get_disease_list():
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    gwas_efo = csv2dict(GWAS_EFO_MAP, '\t')
    tmp_immune_diseases = set([i['Disease trait'] for i in gwas_efo if i['Parent term'] == 'Immune system disorder'])
    all_diseases = set([i['DISEASE/TRAIT'] for i in gwas_caa])
    return all_diseases.intersection(tmp_immune_diseases)

''' #############################  END  ############################## '''


''' This function get a nested dict (ndict) and generates a list of flattened dicts to be inserted within mongodb '''
def flatten_dict2(d, depth):
    if depth == 1:
        for i in d.values():
            yield i
    else:
        for v in d.values():
            if isinstance(v, dict):
                for i in flatten_dict2(v, depth-1):
                    yield i


''' ########### ESSENTIAL CODE: ############# '''


'''
* get_GWAS() returns the list of dict containing AI_related entries.
Such dict can be further used to insert the entries of interest into MongoDB
'''
def get_GWAS(trait_file):
    disease_traits = file2list(trait_file) # Path variable for the list of diseases
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    to_return =  [g for g in gwas_caa if g['DISEASE/TRAIT'] in disease_traits]
    print len(to_return)
    return to_return


''' The following function get both: the list of already pre-processed entries from GWAS (for mongoDB insertion) and the file for SNP conversion
He does prepare a new mongoDB with GWAS converted to hg19 and associated domains.
'''
def SNP_coords_tranform(data_dict, fields_selection):
    new_data_dict = [] 
    SNP_map = pickle_load(SNP_mapping_file)
    for d in data_dict:
       try:
           rs = d['SNP_ID_CURRENT']
           if rs:
               query = ''.join(('rs', rs))
               if SNP_map[query]:
                   SNP_new_chr, SNP_new_pos = SNP_map[query].split("'")[1].split("-")[0].split(":")
                   new_d = {k: d[k] for k in fields_selection}
                   new_d['CHR_ID'] = SNP_new_chr
                   new_d['CHR_POS'] = SNP_new_pos
                   new_data_dict.append(new_d)
               else:
                   print query, 'No data are available for this query'
       except:
           pass
    
    return new_data_dict


'''
Description : ...
'''
def get_domains_dict(domain_dict):
    data_dict = {}
    for k, v in domain_dict.iteritems():
        try:
            data_dict[v['chr']].update({k: v})
        except:
            try:
                data_dict[v['chr']] = {k: v}
            except:
                print "Something is wrong with %s" %k
                break
    return data_dict


'''
Description: WORKING ON THIS FUNCTION
'''
def SNP_TAD_mapping(data_dict, k):
    files = {'CCD': CCD, 'RAO': RAO, 'IMR90': IMR90}
    # A dict containing the dicts of the domain data present in the "files" dict.
    TAD_data = {fn: get_domains_dict(csv2dict_simple(f, '\t')) for fn, f in files.iteritems()} 
    GWAS_legal = [entry for entry in data_dict if entry['CHR_POS']]
    for entry in GWAS_legal:
        entry['DOMAINS'] = []
        chr_ID = ''.join(['chr', entry['CHR_ID']])
        try:
            for tad_id, tad in TAD_data[k][chr_ID].iteritems():
                if int(entry['CHR_POS']) >= int(tad['start']) and int(entry['CHR_POS']) <= int(tad['end']):
                    entry['DOMAINS'].append(tad)
                else:
                    pass
        except:
            pass
        
    return data_dict


def main():
    DB_name = 'GWAS_hg19'
    collection_name = 'GWAS'
    trait_file = ALL_DISEASES
    # trait_file = AI_DISEASE_TRAITS_RESTRICTED 
    data_dict = get_GWAS(trait_file)
    
    ''' This contains the selection of fields to be included in the hg19 database 
    (e.g. downstream, upstream gene distances are excluded because are unknown after the SNP coordinate conversion)'''
    
    fields_selection = ['CNV',
    'OR or BETA',
    'SNP_GENE_IDS',
    'DISEASE/TRAIT',
    'MAPPED_TRAIT_URI',
    'PLATFORM [SNPS PASSING QC]',
    'LINK',
    'CONTEXT',
    'DATE',
    'P-VALUE (TEXT)',
    '95% CI (TEXT)', 
    'FIRST AUTHOR',
    'CHR_ID',
    'INTERGENIC',
    'PUBMEDID',
    'SNP_ID_CURRENT',
    'INITIAL SAMPLE SIZE',
    'MERGED',
    'PVALUE_MLOG',
    'SNPS',
    'MAPPED_TRAIT',
    'STUDY',
    'RISK ALLELE FREQUENCY',
    'CHR_POS',
    'MAPPED_GENE',
    'P-VALUE',
    'STRONGEST SNP-RISK ALLELE',
    'REPORTED GENE(S)',
    'JOURNAL',
    'REPLICATION SAMPLE SIZE',
    'DATE ADDED TO CATALOG',
    'REGION']

    new_data_dict = SNP_coords_tranform(data_dict, fields_selection)
    new_data_dict = SNP_TAD_mapping(new_data_dict, 'CCD')
    
    ''' TODO: 1) Check how many chromosome ID are wrongly annoteated on the whole dataset 
              2) Export into JSON
              3) Re-write query system in ElasticSearch instead of MongoDB queries
    ''' 
    try:
        mongo_drop_collection(DB_name, collection_name)
        mongo_insert(DB_name, collection_name, new_data_dict)
        print '\nThe mongoDB containing the database %s and collection %s was generated.\n' %(DB_name, collection_name)
    except:
        print '\nOps, a problem occoured while building your marvelous mongoDB! Please check what went wrong\n'

if __name__ == '__main__':
    main()
