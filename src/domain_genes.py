'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *


def mongo_request(request_data, db_name, collection_name):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    return list(collection.find(request_data['find']).sort(request_data['sort'], -1))


def write_domain_genes(mongo_response, root_path_dg, dataset, domain_genes, db_name, tmp_domain_genes, domain_genes_outname):
    filename = root_path_dg + dataset + domain_genes_outname
    tmp_out = open(filename, 'w')
    header = "dataset;chr:start-end;total_genes\n"
    tmp_out.write(header)

    dg_file = domain_genes[dataset]
    dg_data = csv2dict(dg_file, '\t')
    mongo_drop_collection(db_name, tmp_domain_genes)
    mongo_insert(db_name, tmp_domain_genes, dg_data)

    try:
        client = MongoClient()
        db = client[db_name]
        collection = db[tmp_domain_genes]
    except:
        print "Unable to connect to MongoDB"

    for m in mongo_response:
        #print m['chr'], m['start'], m['end'], m['AI_hits'], m['AI_genes']
        try:
            sele = collection.aggregate([{'$match': {'chr': m['chr'], 'start': m['start'], 'end': m['end']}}, {'$group': {'_id': {'chr': '$chr', 'start': '$start', 'end': '$end'}, 'GENES': {'$addToSet': '$gene'}}}])  # Other keys(_id), values($DISEASE/TRAIT) may be used for the desired aggregation.
            for i in sele:
                message = "%s;%s:%s-%s;%s\n" %(dataset, i['_id']['chr'], i['_id']['start'], i['_id']['end'], ','.join(i['GENES']))
                tmp_out.write(message)
        except:
            print "Unable to aggregate"

def execute(params, data_case):

    for dataset in params[data_case]['datasets']:
        request_data = {'find': {'dataset': dataset}, # RAO, CCD or IMR90
                        'sort': 'AI_hits'
                        }
    # Mongo request
        mongo_response = mongo_request(request_data, params[data_case]['db_name'], params[data_case]['collection_name'])
    # Write Intra_domain genes
        write_domain_genes(mongo_response, params[data_case]['root_path_dg'], dataset, params[data_case]['domain_genes'], params[data_case]['db_name'], params[data_case]['dg_collection_name'], params[data_case]['domain_genes_outname'])


def main():

    params = {'RA': {'db_name': 'AI_panel',
                     'collection_name': 'AI_domains_RA',
                     'dg_collection_name': 'TMP_DOMAIN_GENES',
                     'datasets': ['CCD', 'IMR90', 'RAO'],
                     'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                     'domain_genes_outname': '_domain_genes_RA.csv',
                     'root_path_dg': DOMAIN_GENES_RES_RA
                     },

              'T1D': {'db_name': 'AI_panel',
                      'collection_name': 'AI_domains_T1D',
                      'dg_collection_name': 'TMP_DOMAIN_GENES',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                      'domain_genes_outname': '_domain_genes_T1D.csv',
                      'root_path_dg': DOMAIN_GENES_RES_T1D
                      },

              'AI': {'db_name': 'AI_panel',
                     'collection_name': 'AI_domains',
                     'dg_collection_name': 'TMP_DOMAIN_GENES',
                     'datasets': ['CCD', 'IMR90', 'RAO'],
                     'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                     'domain_genes_outname': '_domain_genes_AI.csv',
                     'root_path_dg': DOMAIN_GENES_RES_AI
                     },

              'ALL': {'db_name': 'AI_panel',
                      'collection_name': 'domains_ALL',
                      'dg_collection_name': 'TMP_DOMAIN_GENES',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                      'domain_genes_outname': '_domain_genes_ALL.csv',
                      'root_path_dg': DOMAIN_GENES_RES_ALL
                      }
              }

    data_cases = ['ALL']

    for data_case in data_cases:
        execute(params, data_case)

if __name__ == '__main__':
    main()
