from utils import *


######################### Function utilities for one-time use ##############################

# Functions used sporadically, for example functions used to generate an output data file that is further used,
# without the need to execute the function multiple times.

'''
* get_disease_list() returns the list of common immune diseases between the GWAS EFO trait mapping file,
and the catalog of GWAS association file (ontology annotated).
* The auto-immune diseases from this file need to be manually selected (any data available),
editing the 'AI_disease_traits.txt' file
'''

def get_disease_list():
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    gwas_efo = csv2dict(GWAS_EFO_MAP, '\t')
    tmp_immune_diseases = set([i['Disease trait'] for i in gwas_efo if i['Parent term'] == 'Immune system disorder'])
    all_diseases = set([i['DISEASE/TRAIT'] for i in gwas_caa])
    return all_diseases.intersection(tmp_immune_diseases)

############################################################################################

'''
* get_AI_gwas() returns the list of dict containing AI_related entries.
Such dict can be further used to insert the entries of interest into MongoDB
'''

def get_AI_gwas(trait_file):
    ai_disease_traits = file2list(trait_file) # Path variable for the list of diseases
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    return [g for g in gwas_caa if g['DISEASE/TRAIT'] in ai_disease_traits]


def get_domains_dict(domain_dict):
    data_dict = {}
    for k, v in domain_dict.iteritems():
        try:
            data_dict[v['chr']].update({k: v})
        except:
            try:
                data_dict[v['chr']] = {k: v}
            except:
                print "Something is wrong with %s" %k
                break
    return data_dict


def SNP_TAD_mapping(TAD_data_raw, AI_gwas, k):
    AI_gwas_legal = [entry for entry in AI_gwas if entry['CHR_POS']]
    for entry in AI_gwas_legal:
        chr_ID = ''.join(['chr', entry['CHR_ID']])
        for tad_id, tad in TAD_data_raw[chr_ID].iteritems():
            if int(entry['CHR_POS']) >= int(tad['start']) and int(entry['CHR_POS']) <= int(tad['end']):
                TAD_data_raw[chr_ID][tad_id]['AI_hits'] += 1
                tad['AI_diseases'].add(entry['DISEASE/TRAIT'])
                tad['AI_genes'].add(entry['MAPPED_GENE'])
                tad['AI_pos'].add(int(entry['CHR_POS']))
                tad['dataset'] = k
            else:
                pass
    # Filtering out empty data with double dict comprehension
    TAD_data = {k: {k_2: v_2 for k_2, v_2 in v.iteritems() if v_2['AI_hits'] > 0} for k, v in TAD_data_raw.iteritems()}
    return TAD_data


def json_dump(filename, data):
    with open(filename, "w") as filename:
        json.dump(TAD_data, filename, indent=4, separators=(',', ': '))

# This function get a nested dict (ndict) and generates a list of flattened dicts to be inserted within mongodb
def flatten_dict2(d, depth):
    if depth == 1:
        for i in d.values():
            yield i
    else:
        for v in d.values():
            if isinstance(v, dict):
                for i in flatten_dict2(v, depth-1):
                    yield i


def execute(DB_name, collection_name, trait_file):
    AI_gwas = get_AI_gwas(trait_file)
    files = {'CCD': CCD, 'RAO': RAO, 'IMR90': IMR90}
    TAD_data_empty = {fn: get_domains_dict(csv2dict_plus_dict(f, '\t')) for fn, f in files.iteritems()} # A dict containing the dicts of the domain data present in the "files" dict.
    TAD_data = {k: SNP_TAD_mapping(TAD_data_empty[k], AI_gwas, k) for k, v in files.iteritems()}
    #Tranform sets in lists before exporting to json or inserting into MongoDB using nested dict comprehension ()
    TAD_data = {k: {k_2: {k_3: {k_4: (list(v_4) if isinstance(v_4, set) else v_4) for k_4, v_4 in v_3.iteritems()} for k_3, v_3 in v_2.iteritems()} for k_2, v_2 in v.iteritems()} for k, v in TAD_data.iteritems()}


    '''
        The following section is dedicated to export the results in different formats
    '''

    ''' ==> JSON file export'''
    #json_dump(JSON_AI_DOMAINS, TAD_data)

    ''' ==> MongoDB data preparation and insertion'''

    entry_list = [j for j in flatten_dict2(TAD_data, 3)] # List of entry dicts easily insertable on mongodb
    mongo_drop_collection(DB_name, collection_name)
    mongo_insert(DB_name, collection_name, entry_list)


def main():
    DB_name = 'AI_panel'
    collection_name = 'domains_ALL'
    trait_file = ALL_DISEASES
    execute(DB_name, collection_name, trait_file)


if __name__ == '__main__':
    main()