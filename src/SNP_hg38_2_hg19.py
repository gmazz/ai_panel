from utils import *

def get_new_coordinates(SNP_names):
    for SNP in SNP_names.keys():
        print SNP
        server = "http://grch37.rest.ensembl.org"
        ext = ''.join(("/variation/human/", SNP, "?"))
        r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
        try: 
            decoded = r.json()
            SNP_names[SNP] = repr(decoded[u'mappings'][0][u'location'])
        except:
            SNP_names[SNP] = 'invalid'
    
    return SNP_names


def get_original_SNP(): # to correct
    try:
        client = MongoClient()
        db = client[AI_PANEL] # mongo db name
        collection = db[AI_GWAS_ALL] # mongo collection name
    except:
        print "Unable to connect to MongoDB"

    try:
        sele = collection.find({}, {'SNP_ID_CURRENT':1, '_id':0})
        # sele = collection.find({})
        if collection.count() == 0:
            print '\nAttention your collection is empty!!\n'
        return sele
    except:
        print "\nPlease check if the mongo server is up and running\n"


def main():
    sele = get_original_SNP()
    snp_list = [''.join(('rs', d.values()[0])) for d in sele if d.values()[0]]
    snp_names = {k:'' for k in snp_list}
    snp_names = get_new_coordinates(snp_names)
    pickle_save(snp_names, '../data/SNP_hg38_hg19.pkl')

main()

# TODO: 1) just uncomment snp_names, get the new names and integrate the new coords with mongodb data already preset.
#       2) generalize for all GWAS (not AI_PANEL only) and integrate 
