'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *
import numpy as np



def mongo_request(db_name, collection_name, trait_list):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    return list(collection.find({"DISEASE/TRAIT": {"$in": trait_list }}))


def get_anchors_dict(anchors_filename):
    anchors = {}
    with open(anchors_filename, 'r+') as f:
        for i, line in enumerate(f):
            tmp_dict = dict(zip(['chr', 'start', 'end'], line.split('\t')[0:3]))
            tmp_dict['hits'] = 0
            anchors[i] = tmp_dict
    return anchors


def anchors_enritchment_py(anchors, data):
    for k, v in anchors.iteritems():
        for d in data:
            if v['chr'] == 'chr'+d['CHR_ID'] :
               if int(d['CHR_POS']) >= int(v['start']) and int(d['CHR_POS']) <= int(v['end']):
                   v['hits'] = v['hits'] + 1
    return anchors


def anchors_enritchment_MongoRequest(anchors, db_name, collection_name, trait_list):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    for k, v in anchors.iteritems():
        CHR_ID = v['chr'].split('chr')[1]
        response = list(collection.find({"DISEASE/TRAIT": {"$in": trait_list },'CHR_ID': CHR_ID, 'CHR_POS':{'$gt' : v['start'], '$lt' : v['end']}}))
        if response:
            print v, response


def get_array(anchors):
    y = np.array([v['hits'] for v in anchors.itervalues()], dtype=float)
    x = np.array([k for k in anchors.iterkeys()], dtype=float)
    return(x,y)

def enrichment_function(a, b):
    return np.nan_to_num(1-(np.divide((np.float(a) - np.float(b)), np.float(a))))


def plot_SNP_anchors(anchors_data, data_case):
    import matplotlib.pyplot as plt

    anchor_data = anchors_data[data_case]
    anchor_data_all = anchors_data['ALL']
    ef = np.vectorize(enrichment_function)
    enrichment_score_vector = ef(anchor_data_all[1], anchor_data[1])

    plt.xlabel('Anchor')
    plt.ylabel('Enrichment Score')
    plt.title('Enrichment score ' + data_case)
    plt.axis([-100, len(enrichment_score_vector)+1, 0, np.max(enrichment_score_vector)+0.1])
    plt.grid(True)
    plt.plot(anchor_data[0], enrichment_score_vector)
    plt.legend(loc='upper right')
    plt.show()


def execute(params, data_case):
    trait_list = file2list(params[data_case]['trait_list'])
    # Python version (not optimalized)
    mongo_response = mongo_request(params[data_case]['db_name'], params[data_case]['collection_name'], trait_list)
    anchors = get_anchors_dict(CTCF_ANCHORS)
    anchors = anchors_enritchment_py(anchors, mongo_response)
    return anchors 
   

    # Request to mongoDB, way slower
    #anchors_enritchment_MongoRequest(anchors, params[data_case]['db_name'], params[data_case]['collection_name'], trait_list)    
    
    # Writes SNPs as bed files
    #write_snp2bed(mongo_response, params[data_case]['root_path_snp'], params[data_case]['snp_outname'])


''' This function serialize the results into a pickle object '''
def serialize(params, data_cases):
    anchors_data = {}
    for data_case in data_cases:
        print "\nProcessing %s ...\n" %data_case
        anchors = execute(params, data_case)
        anchors_data[data_case] = get_array(anchors)

    # Saving enriched anchors into a pickle object for further easy manipulation.
    pickle_save(anchors_data, ANCHORS_SNP_ENRICHMENT)
    print "\nYour data were serialized in the %s file\n" %ANCHORS_SNP_ENRICHMENT

def main():

    params = {'RA': {'db_name': 'GWAS_hg19',
                     'collection_name': 'GWAS',
                     'trait_list': AI_DISEASE_RA,
                     'snp_outname': 'SNP_RA_hg19.bed',
                     'root_path_snp': SNPs_HG19
                    },

              'T1D': {'db_name': 'GWAS_hg19',
                      'collection_name': 'GWAS',
                      'trait_list': AI_DISEASE_T1D,
                      'snp_outname': 'SNP_T1D_hg19.bed',
                      'root_path_snp': SNPs_HG19
                      },

              'AI': {'db_name': 'GWAS_hg19',
                      'collection_name': 'GWAS',
                      'trait_list': AI_DISEASE_TRAITS_RESTRICTED,
                      'snp_outname': 'SNP_AI_hg19.bed',
                      'root_path_snp': SNPs_HG19
                      },

              'ALL': {'db_name': 'GWAS_hg19',
                      'collection_name': 'GWAS',
                      'trait_list': ALL_DISEASES,
                      'snp_outname': 'SNP_AI_hg19.bed',
                      'root_path_snp': SNPs_HG19
                      }
              }

    data_cases = ['T1D', 'AI', 'RA']

    ''' Function to serialize data '''
    # serialize(params, data_cases)
    
    ''' Use the serialized anchors SNP enrichment data '''
    
    anchors_data = pickle_load(ANCHORS_SNP_ENRICHMENT)
    plot_SNP_anchors(anchors_data, 'T1D') 

if __name__ == '__main__':
    main()
