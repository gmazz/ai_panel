'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *


def mongo_request(request_data, db_name, collection_name):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    return list(collection.find(request_data['find']).sort(request_data['sort'], -1))


def check_external_genes(mongo_response, external_gene_list):
    mongo_response_selected = []
    for ext in external_gene_list:
        for m in mongo_response:
            gene_list = list(set(itertools.chain(*[str(i).split(' - ') for i in m['AI_genes']])))
            #m['AI_genes'] = gene_list
            if ext in gene_list:
                mongo_response_selected.append(m)
    return mongo_response_selected


def check_domain_genes(mongo_response_selected, dataset, domain_genes, db_name, tmp_domain_genes, root_path_external, external_outfile, external_domain_genes_outfile):
    outname1 = root_path_external + dataset + external_outfile
    outname2 = root_path_external + dataset + external_domain_genes_outfile
    out1 = open(outname1, 'w')
    header_1 = 'dataset;ai_hits;chromosome:start-end;AI_genes;AI_diseases\n'
    out1.write(header_1)
    out2 = open(outname2, 'w')
    header_2 = 'dataset;chr:start-end;total_genes\n'
    out2.write(header_2)

    dg_file = domain_genes[dataset]
    dg_data = csv2dict(dg_file, '\t')
    mongo_drop_collection(db_name, tmp_domain_genes)
    mongo_insert(db_name, tmp_domain_genes, dg_data)

    try:
        client = MongoClient()
        db = client[db_name]
        collection = db[tmp_domain_genes]
    except:
        print "Unable to connect to MongoDB"

    for m in mongo_response_selected:
        m1 = "%s;%s;%s:%s-%s;%s;%s\n" %(m['dataset'], m['AI_hits'], m['chr'], m['start'], m['end'], ','.join(m['AI_genes']), ','.join(m['AI_diseases']))
        try:
            sele = collection.aggregate([{'$match': {'chr': m['chr'], 'start': m['start'], 'end': m['end']}}, {'$group': {'_id': {'chr': '$chr', 'start': '$start', 'end': '$end'}, 'GENES': {'$addToSet': '$gene'}}}])  # Other keys(_id), values($DISEASE/TRAIT) may be used for the desired aggregation.
            for i in sele:
                m2 = "%s;%s:%s-%s;%s\n" %(dataset, i['_id']['chr'], i['_id']['start'], i['_id']['end'], ','.join(i['GENES']))

        except:
            print "Unable to aggregate"
            m2 = ''

        out1.write(m1)
        out2.write(m2)

def execute(params, data_case):
    for dataset in params[data_case]['datasets']:
        request_data = {'find': {'dataset': dataset}, # RAO, CCD or IMR90
                        'sort': 'AI_hits'
                        }
    # Mongo request
        mongo_response = mongo_request(request_data, params[data_case]['db_name'], params[data_case]['collection_name'])
    # Write Intra_domain genes
        mongo_response_selected = check_external_genes(mongo_response, params[data_case]['external_gene_list'])
        check_domain_genes(mongo_response_selected,
                           dataset,
                           params[data_case]['domain_genes'],
                           params[data_case]['db_name'],
                           params[data_case]['dg_collection_name'],
                           params[data_case]['root_path_external'],
                           params[data_case]['AI_external_outfile'],
                           params[data_case]['AI_external_domain_genes_outfile']
                           )

def main():
    params = {'RA': {'db_name': 'AI_panel',
                     'collection_name': 'AI_domains_RA',
                     'dg_collection_name': 'TMP_DOMAIN_GENES',
                     'datasets': ['CCD', 'IMR90', 'RAO'],
                     'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                     'external_gene_list': file2list(GENE_LIST_KRYSTIAN)
                     },

              'T1D': {'db_name': 'AI_panel',
                      'collection_name': 'AI_domains_T1D',
                      'dg_collection_name': 'TMP_DOMAIN_GENES',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                      'external_gene_list': file2list(GENE_LIST_KRYSTIAN)
                      },

              'AI': {'db_name': 'AI_panel',
                     'collection_name': 'AI_domains',
                     'dg_collection_name': 'TMP_DOMAIN_GENES',
                     'datasets': ['CCD', 'IMR90', 'RAO'],
                     'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                     'external_gene_list': file2list(GENE_LIST_KRYSTIAN),
                     'root_path_external': DOMAINS_EXTERNAL_GENES_AI,
                     'AI_external_outfile': '_AI_external.csv',
                     'AI_external_domain_genes_outfile': '_AI_external_domain_genes.csv'
                     },

              'ALL': {'db_name': 'AI_panel',
                      'collection_name': 'domains_ALL',
                      'dg_collection_name': 'TMP_DOMAIN_GENES',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'domain_genes': {'CCD': CCD_HG38_GENES, 'IMR90': IMR90_HG38_GENES, 'RAO': RAO_HG38_GENES},
                      'external_gene_list': file2list(GENE_LIST_KRYSTIAN)
                      }
              }

    data_cases = ['AI']

    for data_case in data_cases:
        execute(params, data_case)

if __name__ == '__main__':
    main()

