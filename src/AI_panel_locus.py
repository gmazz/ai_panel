from utils import *

######################### Function utilities for one-time use ##############################

# Functions used sporadically, for example functions used to generate an output data file that is further used,
# without the need to execute the function multiple times.

'''
* get_disease_list() returns the list of common immune diseases between the GWAS EFO trait mapping file,
and the catalog of GWAS association file (ontology annotated).
* The auto-immune diseases from this file need to be manually selected (any data available),
editing the 'AI_disease_traits.txt' file
'''

def get_disease_list():
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    gwas_efo = csv2dict(GWAS_EFO_MAP, '\t')
    tmp_immune_diseases = set([i['Disease trait'] for i in gwas_efo if i['Parent term'] == 'Immune system disorder'])
    all_diseases = set([i['DISEASE/TRAIT'] for i in gwas_caa])
    return all_diseases.intersection(tmp_immune_diseases)

##############################################################################################

'''
* get_AI_gwas() returns the list of dict containing AI_related entries.
Such dict can be further used to insert the entries of interest into MongoDB
'''

def get_AI_gwas(trait_file):
    ai_disease_traits = file2list(trait_file) # Here we can use the list disease variables specified in pat_config.py (e.g. AI_DISEASE_TRAITS) .
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    return [g for g in gwas_caa if g['DISEASE/TRAIT'] in ai_disease_traits]


'''
* The following function insert the AI_gwas into MongoDB and perform an aggregation over their genes
'''
def mongo_insert_AI_gwas(AI_gwas, db_name, collection_name):
    # This function insert a given list of dicts into a specific mongoDB collection
    try:
        client = MongoClient()
        db = client[db_name]
        collection = db[collection_name]
    except:
        print "Unable to connect to MongoDB"
    try:
        collection.insert(AI_gwas)
        print "Your request were inserted in %s.%s" % (db_name, collection_name)
    except:
        print "Unfortunately your request couldn't be inserted in %s.%s"


def mongo_aggregate_AI_gwas(AI_gwas, db_name, collection_name):
    try:
        client = MongoClient()
        db = client[db_name]
        collection = db[collection_name]
    except:
        print "Unable to connect to MongoDB"
    try:
        sele = collection.aggregate([{"$group": {"_id": "$REGION", "DIS": {"$addToSet": "$DISEASE/TRAIT"}, "GENES" : {"$addToSet": "$MAPPED_GENE"}}}]) # Other keys(_id), values($DISEASE/TRAIT) may be used for the desired aggregation.
        return sorted(sele, key=lambda k: len(k['DIS']), reverse=True)

    except:
        print "Unfortunately your request couldn't be inserted in %s.%s"

def print_results(data_res):
    for k in data_res:
        try:
            print "%s; %s; %s; %s; %s" % (k['_id'], len(k['DIS']), len(k['GENES']), ', '.join(k['DIS']), ', '.join(k['GENES']))
        except:
            print "can't print %s" %k
            break

def mongo_drop_collection(db_name, collection_name):
    client = MongoClient()
    db = client[db_name]
    db.drop_collection(collection_name)

def compare_gene_list(data_res, external_data):
    exd = set(file2list(external_data))
    tmp_set = set(itertools.chain.from_iterable([k['GENES'] for k in data_res]))
    intd = set(flatten(map(lambda i: [str(j).split(',') for j in str(i).split(' - ')], tmp_set)))
    return intd.intersection(exd)

def main():
    db_name = 'AI_panel'
    collection_name = 'AI_GWAS_T1D'
    trait_file = AI_DISEASE_T1D
    AI_gwas = get_AI_gwas(trait_file)

    mongo_drop_collection(db_name, collection_name)
    mongo_insert_AI_gwas(AI_gwas, db_name, collection_name)

    # Aggregation:

    # data_res = mongo_aggregate_AI_gwas(AI_gwas, db_name, collection_name)

    #Print Results
    #print compare_gene_list(data_res, GENE_LIST_KRYSTIAN)
    #print_results(data_res)

if __name__ == '__main__':
    main()