'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *


def mongo_request(request, db_name, collection_name, trait_list):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    return list(collection.find({"DISEASE/TRAIT": {"$in": trait_list }}))


def get_message(entry):
    if (entry['CHR_ID'] == 'X') or (entry['CHR_ID'] == 'Y'):
        entry['CHR_ID'] = 23

    message = "%s\t%s\t%s\t%s\n" %(int(entry['CHR_ID']),
                        int(entry['CHR_POS']),
                        int(entry['CHR_POS']),
                        entry['MAPPED_TRAIT'],
                        )
    return str(message)


def write_snp2bed(data, rootpath_snp, snp_outname):
    filename = rootpath_snp + snp_outname
    with open(filename, 'w+') as f:
        for i in data:
            f.write(get_message(i))


def execute(params, data_case):

    request = ''
    trait_list = file2list(params[data_case]['trait_list'])

    # Mongo request
    mongo_response = mongo_request(request, params[data_case]['db_name'], params[data_case]['collection_name'], trait_list)

    # Writes SNPs as bed files
    write_snp2bed(mongo_response, params[data_case]['root_path_snp'], params[data_case]['snp_outname'])


def main():

    params = {'RA': {'db_name': 'GWAS_hg19',
                     'collection_name': 'GWAS',
                     'trait_list': AI_DISEASE_RA,
                     'snp_outname': 'SNP_RA_hg19.bed',
                     'root_path_snp': SNPs_HG19
                    },

              'T1D': {'db_name': 'GWAS_hg19',
                      'collection_name': 'GWAS',
                      'trait_list': AI_DISEASE_T1D,
                      'snp_outname': 'SNP_T1D_hg19.bed',
                      'root_path_snp': SNPs_HG19
                      },

              'AI': {'db_name': 'GWAS_hg19',
                      'collection_name': 'GWAS',
                      'trait_list': AI_DISEASE_TRAITS_RESTRICTED,
                      'snp_outname': 'SNP_AI_hg19.bed',
                      'root_path_snp': SNPs_HG19
                      },

              }

    data_cases = ['T1D', 'AI', 'RA']

    for data_case in data_cases:
        execute(params, data_case)

if __name__ == '__main__':
    main()
