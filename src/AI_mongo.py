'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *


def mongo_request(request_data, db_name, collection_name):
    client = MongoClient()
    db = client[db_name]
    collection = db[collection_name]
    return list(collection.find(request_data['find']).sort(request_data['sort'], -1))


def get_message(entry):
    message = "%s;%s;%s:%s-%s;%s;%s\n" %(entry['dataset'],
                        entry['AI_hits'],
                        entry['chr'],
                        entry['start'],
                        entry['end'],
                        ', '.join(entry['AI_genes']),
                        ', '.join(entry['AI_diseases']),
                        )
    return str(message)


def write_results(data, rootpath_res, dataset, res_outname):
    filename = rootpath_res + dataset + res_outname
    print filename
    tmp_out = open(filename, 'w')
    header = "dataset;ai_hits;chromosome:start-end;AI_genes;AI_diseases\n"
    tmp_out.write(header)
    for entry in data:
        tmp_out.write(entry)


def write_snp2bed(data, rootpath_snp, dataset, snp_outname):
    filename = rootpath_snp + dataset + snp_outname
    tmp_out = open(filename, 'w')

    for i in data:
        snps = [int(j) for j in i['AI_pos']]
        for j in i['AI_pos']:
    #        #tmp_out.write("%s\t%s\n" % (str(i['chr']), int(j)))
            tmp_out.write("%s\t%s\t%s\n" %(str(i['chr']), int(j), int(j)+1))


def execute(params, data_case):

    for dataset in params[data_case]['datasets']:
        request_data = {'find': {'dataset': dataset}, # RAO, CCD or IMR90
                        'sort': 'AI_hits'
                        }
    # Mongo request
        mongo_response = mongo_request(request_data, params[data_case]['db_name'], params[data_case]['collection_name'])

    # Writes SNPs as bed files
        write_snp2bed(mongo_response, params[data_case]['root_path_snp'], dataset, params[data_case]['snp_outname'])

    # Writes AI_TAD_... data
        write_results(map(get_message, mongo_response), params[data_case]['root_path_res'], dataset, params[data_case]['res_outname'])



def main():

    params = {'RA': {'db_name': 'AI_panel',
                     'collection_name': 'AI_domains_RA',
                     'datasets': ['CCD', 'IMR90', 'RAO'],
                     'res_outname': '_AI_RA.csv',
                     'snp_outname': '_SNP_RA_p1.bed',
                     'root_path_res': RESULTS_HG38_RA,
                     'root_path_snp': SNPs_HG38_RA
                    },

              'T1D': {'db_name': 'AI_panel',
                      'collection_name': 'AI_domains_T1D',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'res_outname': '_AI_T1D.csv',
                      'snp_outname': '_SNP_T1D_p1.bed',
                      'root_path_res': RESULTS_HG38_T1D,
                      'root_path_snp': SNPs_HG38_T1D
                      },

              'AI': {'db_name': 'AI_panel',
                      'collection_name': 'AI_domains',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'res_outname': '_AI.csv',
                      'snp_outname': '_SNP_p1.bed',
                      'root_path_res': RESULTS_HG38_AI,
                      'root_path_snp': SNPs_HG38_AI
                      },

              'ALL': {'db_name': 'AI_panel',
                      'collection_name': 'domains_ALL',
                      'datasets': ['CCD', 'IMR90', 'RAO'],
                      'res_outname': '_ALL.csv',
                      'snp_outname': '_SNP_ALL_p1.bed',
                      'root_path_res': RESULTS_HG38_ALL,
                      'root_path_snp': SNPs_HG38_ALL
                      }
              }

    data_cases = ['T1D', 'AI', 'ALL']

    for data_case in data_cases:
        execute(params, data_case)

if __name__ == '__main__':
    main()
