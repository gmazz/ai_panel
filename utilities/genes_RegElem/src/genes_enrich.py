# This program give the list of regulatory elements included within a .bed file including intervals (e.g. domains)
import os, re, sys, subprocess, csv, cPickle

#Global data variables:


REG_data = "../data/wgEncodeAwgSegmentationCombinedGm12878.bed"
domains = "../data/RA_domains.bed"  #"../data/domains.bed"


save_REG_dict = '../data/RA_REG_dict.pkl'
save_REG_mapping_dict = '../data/RA_REG_mapping_dict.pkl'

# Parse csv to data dict
def csv2dict(csv_file, sep):
    input_file = csv.DictReader(open(csv_file), delimiter=sep)
    return [row for row in input_file]


# Aggregates all REG entries by chromosome for faster search ({chr1: [entry1, entry2], chr2: [entry3, entry4]})
def get_REG_dict(REG_data_list):
    REG_dict = {}
    for k in REG_data_list:
        try:
            REG_dict[k['chr']].append(k)
        except:
            try:
                REG_dict[k['chr']] = [k]
            except:
              print "Something is wrong with %s" %k
              break
    return REG_dict


# Saves object by pickle on given path.
def pickle_save(obj, path):
    with open(path, 'wb') as save_file:
        cPickle.dump(obj, save_file)


# Loads object by pickle on given path.
def pickle_load(path):
    with open(path, 'rb') as load_file:
        return cPickle.load(load_file)


def REG_mapping(domain_list, REG_dict):
    REG_mapping_dict = {}
    for dom in domain_list:
        dom_id = '%s_%s_%s' %(dom['chr'], dom['chromStart'], dom['chromEnd'])
        REG_mapping_dict[dom_id] = []
        for entry in REG_dict[dom['chr']]:
            if int(entry['chromStart']) >= int(dom['chromStart']) and int(entry['chromEnd']) <= int(dom['chromEnd']):
                REG_mapping_dict[dom_id].append(entry)
    return REG_mapping_dict


def run_and_serialize():
    domain_list = csv2dict(domains, '\t')
    # Serializes REG_dict data for faster testing, once REG_dict.pkl is active it can be used directly.
    REG_data_list = csv2dict(REG_data, '\t')
    REG_dict = get_REG_dict(REG_data_list)
    pickle_save(REG_dict, save_REG_dict)

    print "Reading REG_dict"
    REG_dict = pickle_load(save_REG_dict)
    print "REG_dict loaded"

    # Running the mapping:
    REG_mapping_dict = REG_mapping(domain_list, REG_dict)

    # Serialize the mapped data, once REG_mapping_dict.pkl is active it can be used directly.
    pickle_save(REG_mapping_dict, save_REG_mapping_dict)


def main():

    run_and_serialize() # This line is required to be run only after the usage of new data
    REG_mapping_dict = pickle_load(save_REG_mapping_dict)

    #Output file:
    outname = '../results/RA_results.bed'
    out_file = open(outname, 'w')
    #header = 'domain\tchromStart\tchromEnd\tname\n'
    #out_file.write(header)

    for k, v in REG_mapping_dict.iteritems():
        for entry in v:
            #message_csv = "%s\t%s\t%s\t%s\n" %(k, entry['chromStart'], entry['chromEnd'], entry['name'])
            message_bed = "%s\t%s\t%s\t%s\t%s\n" %(k.split('_')[0], entry['chromStart'], entry['chromEnd'], entry['name'], entry['itemRgb'])
            out_file.write(message_bed)
main()